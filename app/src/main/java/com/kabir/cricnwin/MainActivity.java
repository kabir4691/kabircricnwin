package com.kabir.cricnwin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.kabir.cricnwin.seating.view.SeatingActivity;
import com.kabir.cricnwin.takenotes.view.TakeNotesActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @OnClick(R.id.take_notes_button)
    void onTakeNotesButtonClicked() {
        startActivity(new Intent(this, TakeNotesActivity.class));
    }

    @OnClick(R.id.seating_button)
    void onSeatingButtonClicked() {
        startActivity(new Intent(this, SeatingActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }
}
