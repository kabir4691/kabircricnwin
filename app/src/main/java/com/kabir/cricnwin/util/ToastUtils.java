package com.kabir.cricnwin.util;

import android.content.Context;
import android.support.annotation.StringRes;
import android.widget.Toast;

import com.kabir.cricnwin.CricNWinApplication;

public class ToastUtils {

    public static void showShort(CharSequence text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
    }

    public static void showShort(@StringRes int resId) {
        Toast.makeText(getContext(), resId, Toast.LENGTH_SHORT).show();
    }

    public static void showLong(CharSequence text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
    }

    public static void showLong(@StringRes int resId) {
        Toast.makeText(getContext(), resId, Toast.LENGTH_LONG).show();
    }

    private static Context getContext() {
        return CricNWinApplication.getInstance();
    }
}
