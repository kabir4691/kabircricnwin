package com.kabir.cricnwin.database.greendao;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.kabir.cricnwin.CricNWinApplication;

import java.util.List;

public class SeatBookingProvider {

    private SeatBookingDao dao;

    public SeatBookingProvider() {
        dao = CricNWinApplication.getDaoSession().getSeatBookingDao();
    }

    private SeatBookingDao getDao() {
        return dao;
    }

    @Nullable
    public SeatBooking get(long id) {
        return getDao().queryBuilder()
                       .where(SeatBookingDao.Properties.Id.eq(id))
                       .unique();
    }

    @NonNull
    public List<SeatBooking> getAll() {
        return getDao().loadAll();
    }

    public void insertOrUpdate(@NonNull SeatBooking seatBooking) {
        SeatBooking entity = get(seatBooking.getId());
        if (entity == null) {
            entity = seatBooking;
        } else {
            entity.overwrite(seatBooking);
        }
        getDao().insertOrReplace(entity);
    }

    public void release() {
        dao.detachAll();
        dao = null;
    }

}
