package com.kabir.cricnwin.database.greendao;

import android.content.Context;
import android.support.annotation.NonNull;

public class CricNWinOpenHelper extends DaoMaster.DevOpenHelper {

    public CricNWinOpenHelper(@NonNull Context context) {
        super(context, "kabir-cricnwin-dev-db");
    }
}
