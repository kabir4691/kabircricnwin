package com.kabir.cricnwin.database.greendao;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.kabir.cricnwin.CricNWinApplication;

import java.util.List;

public class SavedNoteProvider {

    private SavedNoteDao dao;

    public SavedNoteProvider() {
        dao = CricNWinApplication.getDaoSession().getSavedNoteDao();
    }

    private SavedNoteDao getDao() {
        return dao;
    }

    @Nullable
    public SavedNote get(long id) {
        return getDao().queryBuilder()
                       .where(SavedNoteDao.Properties.Id.eq(id))
                       .unique();
    }

    @NonNull
    public List<SavedNote> getAll() {
        return getDao().loadAll();
    }

    public void insertOrUpdate(@NonNull SavedNote note) {
        SavedNote entity = get(note.getId());
        if (entity == null) {
            entity = note;
        } else {
            entity.overwrite(note);
        }
        getDao().insertOrReplace(entity);
    }

    public void delete(long id) {
        SavedNote entity = get(id);
        if (entity != null) {
            getDao().delete(entity);
        }
    }

    public void release() {
        dao.detachAll();
        dao = null;
    }
}
