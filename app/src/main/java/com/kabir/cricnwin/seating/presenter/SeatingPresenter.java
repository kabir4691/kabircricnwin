package com.kabir.cricnwin.seating.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Size;

import com.kabir.cricnwin.seating.model.Seat;

import java.util.List;

public interface SeatingPresenter {

    void initialize();

    void start();

    void onSeatTapped(@NonNull Seat seat);

    void onBookSeatsRequested();
    void onBookSeatsConfirmed(@NonNull @Size(min = 1) List<Seat> seatList);

    void onResetBookingsRequested();
    void onResetBookingsConfirmed();

    void release();
}
