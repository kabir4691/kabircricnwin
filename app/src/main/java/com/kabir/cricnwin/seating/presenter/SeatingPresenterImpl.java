package com.kabir.cricnwin.seating.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Size;

import com.kabir.cricnwin.database.greendao.SeatBooking;
import com.kabir.cricnwin.database.greendao.SeatBookingProvider;
import com.kabir.cricnwin.seating.model.Seat;
import com.kabir.cricnwin.seating.view.SeatingView;

import java.util.ArrayList;
import java.util.List;

public class SeatingPresenterImpl implements SeatingPresenter {

    private SeatingView seatingView;

    private List<Seat> seatList;

    private SeatBookingProvider seatBookingProvider;

    public SeatingPresenterImpl(@NonNull SeatingView seatingView) {
        this.seatingView = seatingView;
    }

    @Override
    public void initialize() {
        seatBookingProvider = new SeatBookingProvider();
        addSeatsIfNotPresent();

    }

    private void addSeatsIfNotPresent() {
        if (seatBookingProvider.getAll().isEmpty()) {
            for (int i = 0; i < 20; i++) {
                long id = i + 1;
                SeatBooking seatBooking = new SeatBooking(id, String.valueOf(id), false);
                seatBookingProvider.insertOrUpdate(seatBooking);
            }
        }
    }

    @Override
    public void start() {
        seatList = new ArrayList<>();
        List<SeatBooking> seatBookingList = seatBookingProvider.getAll();
        for (SeatBooking seatBooking : seatBookingList) {
            Seat seat = new Seat(seatBooking.getId(), seatBooking.getName(),
                                 seatBooking.getIsBooked() ? Seat.Status.BOOKED : Seat.Status.AVAILABLE);
            seatList.add(seat);
        }
        seatingView.showSeats(seatList);

        checkBookSeatsValidity();
        checkResetBookingsValidity();
    }

    private void checkBookSeatsValidity() {
        boolean isValid = false;
        for (Seat seat : seatList) {
            if (seat.getStatus() == Seat.Status.SELECTED) {
                isValid = true;
                break;
            }
        }
        seatingView.setBookSeatsEnabled(isValid);
    }

    private void checkResetBookingsValidity() {
        boolean isResetBookingsValid = false;
        for (Seat seat : seatList) {
            if (seat.getStatus() == Seat.Status.BOOKED) {
                isResetBookingsValid = true;
                break;
            }
        }
        seatingView.setResetBookingsEnabled(isResetBookingsValid);
    }

    @Override
    public void onSeatTapped(@NonNull Seat tappedSeat) {
        switch (tappedSeat.getStatus()) {
            case AVAILABLE:
                tappedSeat.setStatus(Seat.Status.SELECTED);
                break;
            case SELECTED:
                tappedSeat.setStatus(Seat.Status.AVAILABLE);
                break;
        }
        seatingView.showSeats(seatList);

        checkBookSeatsValidity();
    }

    @Override
    public void onBookSeatsRequested() {
        List<Seat> selectedSeatList = new ArrayList<>();
        for (Seat seat : seatList) {
            if (seat.getStatus() == Seat.Status.SELECTED) {
                selectedSeatList.add(seat);
            }
        }

        seatingView.showBookSeatsConfirmation(selectedSeatList);
    }

    @Override
    public void onBookSeatsConfirmed(@NonNull @Size(min = 1) List<Seat> seatList) {
        for (Seat seat : seatList) {
            SeatBooking seatBooking = new SeatBooking(seat.getId(), seat.getName(), true);
            seatBookingProvider.insertOrUpdate(seatBooking);
        }
        start();
    }

    @Override
    public void onResetBookingsRequested() {
        seatingView.showResetBookingsConfirmation();
    }

    @Override
    public void onResetBookingsConfirmed() {
        List<SeatBooking> list = seatBookingProvider.getAll();
        for (SeatBooking seatBooking : list) {
            SeatBooking seatBooking2 = new SeatBooking(seatBooking.getId(), seatBooking.getName(), false);
            seatBookingProvider.insertOrUpdate(seatBooking2);
        }
        start();
    }

    @Override
    public void release() {
        seatBookingProvider.release();
    }
}
