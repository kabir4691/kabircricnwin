package com.kabir.cricnwin.seating.view;

import android.support.annotation.NonNull;
import android.support.annotation.Size;

import com.kabir.cricnwin.seating.model.Seat;

import java.util.List;

public interface SeatingView {

    void showSeats(@NonNull List<Seat> seatList);

    void setBookSeatsEnabled(boolean isEnabled);
    void setResetBookingsEnabled(boolean isEnabled);

    void showBookSeatsConfirmation(@NonNull @Size(min = 1) List<Seat> seatList);
    void showResetBookingsConfirmation();
}
