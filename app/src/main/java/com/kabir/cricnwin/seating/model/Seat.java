package com.kabir.cricnwin.seating.model;

import android.support.annotation.NonNull;

public class Seat {

    private long id;
    private String name;
    private Status status;

    public enum Status {
        AVAILABLE,
        SELECTED,
        BOOKED
    }

    public Seat(long id, @NonNull String name, @NonNull Status status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NonNull Status status) {
        this.status = status;
    }
}
