package com.kabir.cricnwin.seating.view;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kabir.cricnwin.R;
import com.kabir.cricnwin.seating.model.Seat;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SeatsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater layoutInflater;
    private Callback callback;

    private List<Unbinder> unbinderList;

    private List<Seat> seatList;

    private int availableColor;
    private int selectedColor;
    private int bookedColor;

    SeatsAdapter(@NonNull LayoutInflater layoutInflater, @NonNull Callback callback) {
        this.layoutInflater = layoutInflater;
        this.callback = callback;
        unbinderList = new ArrayList<>();
        seatList = new ArrayList<>();
        availableColor = Color.parseColor("#00FF00");
        selectedColor = Color.parseColor("#0000FF");
        bookedColor = Color.parseColor("#FF0000");
    }

    @NonNull @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SeatViewHolder(layoutInflater.inflate(R.layout.layout_seating_seat, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Seat seat = seatList.get(position);
        SeatViewHolder holder = (SeatViewHolder) viewHolder;
        switch (seat.getStatus()) {
            case AVAILABLE:
                holder.seatImageView.setBackgroundColor(availableColor);
                break;
            case SELECTED:
                holder.seatImageView.setBackgroundColor(selectedColor);
                break;
            case BOOKED:
                holder.seatImageView.setBackgroundColor(bookedColor);
                break;
        }
        holder.nameTextView.setText(seat.getName());
    }

    @Override
    public int getItemCount() {
        return seatList.size();
    }

    void unbindViews() {
        if (unbinderList != null) {
            for (Unbinder unbinder : unbinderList) {
                unbinder.unbind();
            }
        }
    }

    void updateSeats(@NonNull List<Seat> seatList) {
        this.seatList = seatList;
        notifyDataSetChanged();
    }

    class SeatViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.seat_image_view) ImageView seatImageView;
        @BindView(R.id.name_text_view) TextView nameTextView;

        private SeatViewHolder(View itemView) {
            super(itemView);

            Unbinder unbinder = ButterKnife.bind(this, itemView);
            unbinderList.add(unbinder);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onSeatClicked(seatList.get(getAdapterPosition()));
                }
            });
        }
    }

    interface Callback {

        void onSeatClicked(@NonNull Seat seat);
    }
}
