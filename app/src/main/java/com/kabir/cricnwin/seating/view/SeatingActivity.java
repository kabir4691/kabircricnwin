package com.kabir.cricnwin.seating.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Size;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.widget.Button;

import com.kabir.cricnwin.R;
import com.kabir.cricnwin.seating.model.Seat;
import com.kabir.cricnwin.seating.presenter.SeatingPresenter;
import com.kabir.cricnwin.seating.presenter.SeatingPresenterImpl;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SeatingActivity extends AppCompatActivity implements SeatingView {

    @BindView(R.id.seats_recycler_view)
    RecyclerView seatsRecyclerView;
    @BindView(R.id.book_seats_button)
    Button bookSeatsButton;
    @BindView(R.id.reset_bookings_button)
    Button resetBookingsButton;

    @OnClick(R.id.book_seats_button)
    void onBookSeatsClicked() {
        presenter.onBookSeatsRequested();
    }

    @OnClick(R.id.reset_bookings_button)
    void onResetBookingsClicked() {
        presenter.onResetBookingsRequested();
    }

    private Unbinder unbinder;

    private SeatsAdapter seatsAdapter;

    private SeatingPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_seating);
        unbinder = ButterKnife.bind(this);

        seatsAdapter = new SeatsAdapter(LayoutInflater.from(this), new SeatsAdapter.Callback() {
            @Override
            public void onSeatClicked(@NonNull Seat seat) {
                presenter.onSeatTapped(seat);
            }
        });
        seatsRecyclerView.setAdapter(seatsAdapter);

        LayoutManager layoutManager = new GridLayoutManager(this, 5, LinearLayoutManager.VERTICAL, false);
        seatsRecyclerView.setLayoutManager(layoutManager);

        presenter = new SeatingPresenterImpl(this);
        presenter.initialize();
        presenter.start();
    }

    @Override
    public void showSeats(@NonNull List<Seat> seatList) {
        seatsAdapter.updateSeats(seatList);
    }

    @Override
    public void setBookSeatsEnabled(boolean isEnabled) {
        bookSeatsButton.setEnabled(isEnabled);
    }

    @Override
    public void setResetBookingsEnabled(boolean isEnabled) {
        resetBookingsButton.setEnabled(isEnabled);
    }

    @Override
    public void showBookSeatsConfirmation(@NonNull @Size(min = 1) final List<Seat> seatList) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < seatList.size(); i++) {
            stringBuilder.append(seatList.get(i).getName());
            if (i != seatList.size() - 1) {
                stringBuilder.append(",");
            }
        }
        String title = "Are you sure you wish to confirm booking " + stringBuilder.toString() + "?";
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.onBookSeatsConfirmed(seatList);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface
                        .OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    @Override
    public void showResetBookingsConfirmation() {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Are you sure you wish to reset all bookings?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.onResetBookingsConfirmed();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface
                        .OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        presenter.release();
        super.onDestroy();
    }
}
