package com.kabir.cricnwin;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.kabir.cricnwin.database.greendao.CricNWinOpenHelper;
import com.kabir.cricnwin.database.greendao.DaoMaster;
import com.kabir.cricnwin.database.greendao.DaoSession;

public class CricNWinApplication extends Application {

    private static CricNWinApplication instance;
    private static DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        initializeDao();
    }

    private void initializeDao() {
        DaoMaster.OpenHelper helper = new CricNWinOpenHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }

    public static synchronized CricNWinApplication getInstance() {
        return instance;
    }

    public static DaoSession getDaoSession() {
        return daoSession;
    }
}
