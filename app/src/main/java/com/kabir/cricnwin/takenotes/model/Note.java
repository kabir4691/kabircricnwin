package com.kabir.cricnwin.takenotes.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

public class Note implements Parcelable {

    private long id;
    private String title;
    private String message;

    public Note(long id, @NonNull String title, @NonNull String message) {
        this.id = id;
        this.title = title;
        this.message = message;
    }

    public Note(@NonNull String title, @NonNull String message) {
        id = System.currentTimeMillis();
        this.title = title;
        this.message = message;
    }

    protected Note(Parcel in) {
        id = in.readLong();
        title = in.readString();
        message = in.readString();
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    public long getId() {
        return id;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public String getMessage() {
        return message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeString(message);
    }
}
