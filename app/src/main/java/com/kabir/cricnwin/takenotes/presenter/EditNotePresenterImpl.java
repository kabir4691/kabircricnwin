package com.kabir.cricnwin.takenotes.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.kabir.cricnwin.takenotes.model.Note;
import com.kabir.cricnwin.takenotes.view.EditNoteView;
import com.kabir.cricnwin.util.ToastUtils;

public class EditNotePresenterImpl implements EditNotePresenter {

    private EditNoteView editNoteView;

    private Note note;

    public EditNotePresenterImpl(@NonNull EditNoteView editNoteView) {
        this.editNoteView = editNoteView;
    }

    @Override
    public boolean initialize(Bundle extras) {
        if (extras == null || extras.isEmpty()) {
            return false;
        }

        note = extras.getParcelable(EditNoteView.EXTRA_NOTE);
        if (note == null) {
            return false;
        }

        return true;
    }

    @Override
    public void start() {
        editNoteView.setTitle(note.getTitle());
        editNoteView.setMessage(note.getMessage());
    }

    @Override
    public void onUpdateNoteRequested(@NonNull String title, @NonNull String message) {
        if (title.isEmpty()) {
            ToastUtils.showShort("Title cannot be empty");
            return;
        }

        if (message.isEmpty()) {
            ToastUtils.showShort("Message cannot be empty");
            return;
        }

        editNoteView.exitAndUpdateNote(title, message);
    }
}
