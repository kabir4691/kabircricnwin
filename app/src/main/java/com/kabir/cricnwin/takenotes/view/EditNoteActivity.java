package com.kabir.cricnwin.takenotes.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.kabir.cricnwin.R;
import com.kabir.cricnwin.takenotes.model.Note;
import com.kabir.cricnwin.takenotes.presenter.EditNotePresenter;
import com.kabir.cricnwin.takenotes.presenter.EditNotePresenterImpl;
import com.kabir.cricnwin.util.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class EditNoteActivity extends AppCompatActivity implements EditNoteView {

    @BindView(R.id.title_edit_text)
    EditText titleEditText;
    @BindView(R.id.message_edit_text)
    EditText messageEditText;

    @OnClick(R.id.update_note_button)
    void onUpdateNoteClicked() {
        presenter.onUpdateNoteRequested(titleEditText.getText().toString(),
                                        messageEditText.getText().toString());
    }

    private Unbinder unbinder;

    private EditNotePresenter presenter;

    @NonNull
    static Bundle generateExtras(@NonNull Note note) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(EXTRA_NOTE, note);
        return bundle;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_note);
        unbinder = ButterKnife.bind(this);

        presenter = new EditNotePresenterImpl(this);
        if (!presenter.initialize(getIntent().getExtras())) {
            ToastUtils.showShort("An unexpected error had occurred");
            supportFinishAfterTransition();
            return;
        }
        presenter.start();
    }

    @Override
    public void setTitle(@NonNull String title) {
        titleEditText.setText(title);
    }

    @Override
    public void setMessage(@NonNull String message) {
        messageEditText.setText(message);
    }

    @Override
    public void exitAndUpdateNote(@NonNull String title, @NonNull String message) {
        Intent data = new Intent();
        data.putExtra(EXTRA_TITLE, title);
        data.putExtra(EXTRA_MESSAGE, message);
        setResult(RESULT_OK, data);
        supportFinishAfterTransition();
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
