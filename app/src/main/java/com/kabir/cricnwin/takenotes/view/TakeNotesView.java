package com.kabir.cricnwin.takenotes.view;

import android.support.annotation.NonNull;

import com.kabir.cricnwin.takenotes.model.Note;

import java.util.List;

public interface TakeNotesView {

    int REQUEST_CODE_EDIT_NOTE = 2;

    void showNotes(@NonNull List<Note> list);
    void clearAddNoteForm();

    void navigateToEditNote(@NonNull Note note);
}
