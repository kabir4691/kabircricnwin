package com.kabir.cricnwin.takenotes.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kabir.cricnwin.R;
import com.kabir.cricnwin.takenotes.model.Note;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

class NotesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater layoutInflater;
    private Callback callback;

    private List<Unbinder> unbinderList;
    private List<AdapterItem> adapterItemList;

    private List<Note> noteList;
    private boolean isAddFormToBeCleared;

    NotesAdapter(@NonNull LayoutInflater layoutInflater, @NonNull Callback callback) {
        this.layoutInflater = layoutInflater;
        this.callback = callback;
        unbinderList = new ArrayList<>();
        noteList = new ArrayList<>();
        adapterItemList = generateAdapterItemList();
    }

    @NonNull @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ViewType.ADD_NOTE.ordinal()) {
            return new AddNoteViewHolder(
                    layoutInflater.inflate(R.layout.layout_take_notes_add_note, parent, false));
        } else if (viewType == ViewType.EXISTING_NOTE.ordinal()) {
            return new ExistingNoteViewHolder(
                    layoutInflater.inflate(R.layout.layout_take_notes_existing_note, parent, false));
        }
        throw new IllegalArgumentException("Illegal view type: " + viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AdapterItem adapterItem = adapterItemList.get(position);
        ViewType viewType = adapterItem.viewType;

        if (viewType == ViewType.ADD_NOTE) {
            AddNoteViewHolder holder = (AddNoteViewHolder) viewHolder;
            if (isAddFormToBeCleared) {
                holder.titleEditText.setText("");
                holder.messageEditText.setText("");
                isAddFormToBeCleared = false;
            }
        } else if (viewType == ViewType.EXISTING_NOTE) {
            ExistingNoteViewHolder holder = (ExistingNoteViewHolder) viewHolder;
            Note note = adapterItem.note;
            holder.titleTextView.setText(note.getTitle());
            holder.messageTextView.setText(note.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return adapterItemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return adapterItemList.get(position).viewType.ordinal();
    }

    void unbindViews() {
        if (unbinderList != null) {
            for (Unbinder unbinder : unbinderList) {
                unbinder.unbind();
            }
        }
    }

    private enum ViewType {
        ADD_NOTE,
        EXISTING_NOTE
    }

    private class AdapterItem {

        private ViewType viewType;
        private Note note;

        private AdapterItem(@NonNull ViewType viewType) {
            this.viewType = viewType;
        }

        private void setNote(@NonNull Note note) {
            this.note = note;
        }
    }

    @NonNull
    private List<AdapterItem> generateAdapterItemList() {
        List<AdapterItem> list = new ArrayList<>();

        if (!noteList.isEmpty()) {
            for (Note note : noteList) {
                AdapterItem adapterItem = new AdapterItem(ViewType.EXISTING_NOTE);
                adapterItem.setNote(note);
                list.add(adapterItem);
            }
        }

        list.add(new AdapterItem(ViewType.ADD_NOTE));

        return list;
    }

    void updateNoteList(@NonNull List<Note> noteList) {
        this.noteList = noteList;
        adapterItemList = generateAdapterItemList();
        notifyDataSetChanged();
    }

    void clearAddNoteForm() {
        isAddFormToBeCleared = true;
        notifyItemChanged(adapterItemList.size() - 1);
    }

    class AddNoteViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title_edit_text) EditText titleEditText;
        @BindView(R.id.message_edit_text) EditText messageEditText;
        @BindView(R.id.add_note_button) Button addNoteButton;

        private AddNoteViewHolder(View itemView) {
            super(itemView);

            Unbinder unbinder = ButterKnife.bind(this, itemView);
            unbinderList.add(unbinder);

            addNoteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onAddNoteClicked(titleEditText.getText().toString(),
                                              messageEditText.getText().toString());
                }
            });
        }
    }

    class ExistingNoteViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title_text_view) TextView titleTextView;
        @BindView(R.id.message_text_view) TextView messageTextView;
        @BindView(R.id.edit_image_button) ImageButton editImageButton;
        @BindView(R.id.delete_image_button) ImageButton deleteImageButton;

        private ExistingNoteViewHolder(View itemView) {
            super(itemView);

            Unbinder unbinder = ButterKnife.bind(this, itemView);
            unbinderList.add(unbinder);

            editImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AdapterItem adapterItem = adapterItemList.get(getAdapterPosition());
                    callback.onEditClicked(adapterItem.note);
                }
            });

            deleteImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AdapterItem adapterItem = adapterItemList.get(getAdapterPosition());
                    callback.onDeleteClicked(adapterItem.note);
                }
            });
        }
    }

    interface Callback {

        void onEditClicked(@NonNull Note note);
        void onDeleteClicked(@NonNull Note note);
        void onAddNoteClicked(@NonNull String title, @NonNull String message);
    }
}
