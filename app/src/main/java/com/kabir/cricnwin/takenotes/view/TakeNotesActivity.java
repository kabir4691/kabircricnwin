package com.kabir.cricnwin.takenotes.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import com.kabir.cricnwin.R;
import com.kabir.cricnwin.takenotes.model.Note;
import com.kabir.cricnwin.takenotes.presenter.TakeNotesPresenter;
import com.kabir.cricnwin.takenotes.presenter.TakesNotesPresenterImpl;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TakeNotesActivity extends AppCompatActivity implements TakeNotesView {

    @BindView(R.id.content_recycler_view)
    RecyclerView contentRecyclerView;

    private Unbinder unbinder;

    private NotesAdapter notesAdapter;

    private TakeNotesPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_take_notes);
        unbinder = ButterKnife.bind(this);

        notesAdapter = new NotesAdapter(LayoutInflater.from(this), new NotesAdapter.Callback() {
            @Override
            public void onEditClicked(@NonNull Note note) {
                presenter.onEditNoteRequested(note);
            }

            @Override
            public void onDeleteClicked(@NonNull final Note note) {
                new AlertDialog.Builder(TakeNotesActivity.this)
                        .setCancelable(false)
                        .setTitle("Are you sure you wish to delete this note?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                presenter.onDeleteNoteRequested(note);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface
                                .OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
            }

            @Override
            public void onAddNoteClicked(@NonNull String title, @NonNull String message) {
                presenter.onAddNoteRequested(title, message);
            }
        });
        contentRecyclerView.setAdapter(notesAdapter);

        contentRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        presenter = new TakesNotesPresenterImpl(this);
        presenter.initialize();
        presenter.start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_EDIT_NOTE && resultCode == RESULT_OK) {
            String title = data.getStringExtra(EditNoteActivity.EXTRA_TITLE);
            String message = data.getStringExtra(EditNoteActivity.EXTRA_MESSAGE);
            presenter.onDataSubmittedForNoteEditing(title, message);
        }
    }

    @Override
    public void showNotes(@NonNull List<Note> list) {
        notesAdapter.updateNoteList(list);
    }

    @Override
    public void clearAddNoteForm() {
        notesAdapter.clearAddNoteForm();
    }

    @Override
    public void navigateToEditNote(@NonNull Note note) {
        Intent intent = new Intent(this, EditNoteActivity.class);
        intent.putExtras(EditNoteActivity.generateExtras(note));
        startActivityForResult(intent, REQUEST_CODE_EDIT_NOTE);
    }

    @Override
    protected void onDestroy() {
        notesAdapter.unbindViews();
        unbinder.unbind();
        presenter.release();
        super.onDestroy();
    }
}
