package com.kabir.cricnwin.takenotes.presenter;

import android.support.annotation.NonNull;

import com.kabir.cricnwin.takenotes.model.Note;

public interface TakeNotesPresenter {

    void initialize();

    void start();

    void onAddNoteRequested(@NonNull String title, @NonNull String message);
    void onEditNoteRequested(@NonNull Note note);
    void onDataSubmittedForNoteEditing(@NonNull String title, @NonNull String message);
    void onDeleteNoteRequested(@NonNull Note note);

    void release();
}
