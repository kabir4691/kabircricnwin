package com.kabir.cricnwin.takenotes.presenter;

import android.support.annotation.NonNull;

import com.kabir.cricnwin.database.greendao.SavedNote;
import com.kabir.cricnwin.database.greendao.SavedNoteProvider;
import com.kabir.cricnwin.takenotes.model.Note;
import com.kabir.cricnwin.takenotes.view.TakeNotesView;
import com.kabir.cricnwin.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

public class TakesNotesPresenterImpl implements TakeNotesPresenter {

    private TakeNotesView takeNotesView;

    private List<Note> currentNoteList;

    private SavedNoteProvider savedNoteProvider;

    private long idOfNoteToBeEdited = -1;

    public TakesNotesPresenterImpl(@NonNull TakeNotesView takeNotesView) {
        this.takeNotesView = takeNotesView;
    }

    @Override
    public void initialize() {
        savedNoteProvider = new SavedNoteProvider();
    }

    @Override
    public void start() {
        List<SavedNote> savedNoteList = savedNoteProvider.getAll();
        if (savedNoteList.isEmpty()) {
            currentNoteList = new ArrayList<>();
        } else {
            List<Note> noteList = new ArrayList<>();
            for (SavedNote savedNote : savedNoteList) {
                Note note = new Note(savedNote.getId(), savedNote.getTitle(), savedNote.getMessage());
                noteList.add(note);
            }
            currentNoteList = noteList;
        }
        takeNotesView.showNotes(currentNoteList);
    }

    @Override
    public void onAddNoteRequested(@NonNull String title, @NonNull String message) {
        if (title.isEmpty()) {
            ToastUtils.showShort("Title cannot be empty");
            return;
        }

        if (message.isEmpty()) {
            ToastUtils.showShort("Message cannot be empty");
            return;
        }

        Note note = new Note(title, message);

        SavedNote savedNote = new SavedNote(note);
        savedNoteProvider.insertOrUpdate(savedNote);

        currentNoteList.add(note);
        takeNotesView.showNotes(currentNoteList);

        takeNotesView.clearAddNoteForm();
    }

    @Override
    public void onEditNoteRequested(@NonNull Note note) {
        takeNotesView.navigateToEditNote(note);
        idOfNoteToBeEdited = note.getId();
    }

    @Override
    public void onDataSubmittedForNoteEditing(@NonNull String title, @NonNull String message) {
        if (savedNoteProvider.get(idOfNoteToBeEdited) == null) {
            ToastUtils.showShort("Note no longer exists");
            start();
            return;
        }

        Note note = new Note(idOfNoteToBeEdited, title, message);
        SavedNote savedNote = new SavedNote(note);
        savedNoteProvider.insertOrUpdate(savedNote);

        ToastUtils.showShort("Note edited successfully");

        start();
    }

    @Override
    public void onDeleteNoteRequested(@NonNull Note note) {
        currentNoteList.remove(note);
        takeNotesView.showNotes(currentNoteList);

        savedNoteProvider.delete(note.getId());
    }

    @Override
    public void release() {
        savedNoteProvider.release();
    }
}
