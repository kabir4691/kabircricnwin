package com.kabir.cricnwin.takenotes.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

public interface EditNotePresenter {

    boolean initialize(Bundle extras);

    void start();

    void onUpdateNoteRequested(@NonNull String title, @NonNull String message);
}
