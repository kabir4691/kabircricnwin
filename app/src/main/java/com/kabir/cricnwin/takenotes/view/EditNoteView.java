package com.kabir.cricnwin.takenotes.view;

import android.support.annotation.NonNull;

public interface EditNoteView {

    String EXTRA_NOTE = "note";
    String EXTRA_TITLE = "title";
    String EXTRA_MESSAGE = "message";

    void setTitle(@NonNull String title);
    void setMessage(@NonNull String message);

    void exitAndUpdateNote(@NonNull String title, @NonNull String message);
}
